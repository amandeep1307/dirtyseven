package dirtyseven.teamoxio.com.dirtyseven;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Multiplayer extends Activity {

    List<ImageView> all_images ;
    List<ImageView> player_one;
    List<ImageView> player_two;
    List<ImageView> open_deck;
    List<ImageView> closed_deck;

    LinearLayout layout_player1;
    LinearLayout layout_player2;

    List<Integer> open_card_res_id;

    TextView start;
    ImageView openImage;
    ImageView closeImage;

    int counterPlayer1=0, counterPlayer2=0;

    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_multiplayer);

        all_images = new ArrayList<ImageView>();
        player_one = new ArrayList<ImageView>();
        player_two = new ArrayList<ImageView>();
        open_deck = new ArrayList<ImageView>();
        closed_deck = new ArrayList<ImageView>();

        start = (TextView)findViewById(R.id.start);
        openImage = (ImageView)findViewById(R.id.open_card);
        closeImage = (ImageView)findViewById(R.id.close_card);

        open_card_res_id = new ArrayList<Integer>();

        counter = 0;

        int[] images_arr = {R.drawable.spade_1, R.drawable.spade_2,R.drawable.spade_3,R.drawable.spade_4,R.drawable.spade_5,
                R.drawable.spade_6,R.drawable.spade_7,R.drawable.spade_8,R.drawable.spade_9,R.drawable.spade_10,
                R.drawable.spade_11,R.drawable.spade_12,R.drawable.spade_13,
                R.drawable.club_1,R.drawable.club_2,R.drawable.club_3,R.drawable.club_4,R.drawable.club_5,
                R.drawable.club_6,R.drawable.club_7,R.drawable.club_8,R.drawable.club_9,R.drawable.club_10,
                R.drawable.club_11,R.drawable.club_12,R.drawable.club_13,
                R.drawable.diamond_1,R.drawable.diamond_2,R.drawable.diamond_3,R.drawable.diamond_4,R.drawable.diamond_5,
                R.drawable.diamond_6,R.drawable.diamond_7,R.drawable.diamond_8,R.drawable.diamond_9,R.drawable.diamond_10,
                R.drawable.diamond_11,R.drawable.diamond_12,R.drawable.diamond_13,
                R.drawable.heart_1, R.drawable.heart_2, R.drawable.heart_3, R.drawable.heart_4, R.drawable.heart_5,
                R.drawable.heart_6, R.drawable.heart_7, R.drawable.heart_8, R.drawable.heart_9, R.drawable.heart_10,
                R.drawable.heart_11, R.drawable.heart_12, R.drawable.heart_13,};


        for(int i = 0; i<images_arr.length; i++)
        {
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(images_arr[i]);
            imageView.setTag(images_arr[i]);
                    //(ImageView)findViewById(images_arr[i]);
            all_images.add(imageView);
        }

        Collections.shuffle(all_images);

        closed_deck = all_images;

        for(int i = 1; i<=7; i++)
        {
            player_one.add(closed_deck.get(closed_deck.size()-1));
            closed_deck.remove(closed_deck.size()-1);
        }

        for(int i = 1; i<=7; i++)
        {
            player_two.add(closed_deck.get(closed_deck.size()-1));
            closed_deck.remove(closed_deck.size()-1);
        }

        open_deck.add(closed_deck.get(closed_deck.size()-1));
        closed_deck.remove(closed_deck.size() - 1);

        layout_player1 = (LinearLayout)findViewById(R.id.layout_player1);
        layout_player2 = (LinearLayout)findViewById(R.id.layout_player2);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(counter%2==0)
            {
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(60, 80);
                player_one.add(closed_deck.get(closed_deck.size()-1));
                player_one.get(player_one.size()-1).setLayoutParams(params);

                final int  temp = player_one.size()-1;
                player_one.get(player_one.size()-1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(counter%2 == 0) {
                            if (CheckMove(Integer.parseInt(player_one.get(temp).getTag().toString()))) {
                                openImage.setImageResource(Integer.parseInt(player_one.get(temp).getTag().toString()));
                                open_deck.add(player_one.get(temp));
                                layout_player1.removeView(player_one.get(temp));
                               counterPlayer1--;
                                checkWin();
                                //                              player_one.remove(player_one.get(temp));
                                counter++;
                            }
                        }
                        else
                                Toast.makeText(getApplicationContext(), "Player 2 Turn", Toast.LENGTH_SHORT).show();
                    }
                });
                layout_player1.addView(closed_deck.get(closed_deck.size() - 1));
                counterPlayer1++;
                closed_deck.remove(closed_deck.size()-1);
                checkCloseDeck();

            }
            if(counter%2!=0)
            {
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(60, 80);
                player_two.add(closed_deck.get(closed_deck.size()-1));
                player_two.get(player_two.size()-1).setLayoutParams(params);
                final int  temp = player_two.size()-1;
                player_two.get(player_two.size()-1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(counter%2 != 0) {
                            if (CheckMove(Integer.parseInt(player_two.get(temp).getTag().toString()))) {
                                openImage.setImageResource(Integer.parseInt(player_two.get(temp).getTag().toString()));
                                open_deck.add(player_two.get(temp));
                                layout_player2.removeView(player_two.get(temp));
                                counterPlayer2--;
                                checkWin();
//                                player_two.remove(player_two.get(temp));
                                counter++;
                            }
                        }
                        else
                                Toast.makeText(getApplicationContext(), "Player 1 Turn", Toast.LENGTH_SHORT).show();
                    }
                });
                layout_player2.addView(closed_deck.get(closed_deck.size() - 1));
                counterPlayer1++;
                closed_deck.remove(closed_deck.size() - 1);
                checkCloseDeck();
            }
            counter++;
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
  //              startfun();

                start.setVisibility(View.GONE);
                counter = 2;
                counterPlayer1 = 7;
                counterPlayer2 = 7;
                openImage.setImageResource(Integer.parseInt(open_deck.get(0).getTag().toString()));
              //  open_card_res_id.add(Integer.parseInt(open_deck.get(0).getTag().toString()));

                for(int i = 0; i<player_one.size(); i++)
                {
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(60, 80);
                    player_one.get(i).setLayoutParams(params);
//                    player_one.get(i).setId(R.id.start);
                    final int temp = i;
                    player_one.get(i).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //open_card_res_id.add(Integer.parseInt(player_one.get(temp).getTag().toString()));
                            if(counter%2 == 0) {
                                if (CheckMove(Integer.parseInt(player_one.get(temp).getTag().toString()))) {
                                    openImage.setImageResource(Integer.parseInt(player_one.get(temp).getTag().toString()));
                                    open_deck.add(player_one.get(temp));
                                    layout_player1.removeView(player_one.get(temp));
                                    //     player_one.remove(player_one.get(temp));
                                    counterPlayer1--;
                                    checkWin();
                                    counter++;
                                }
                            }
                           else
                               Toast.makeText(getApplicationContext(), "Player 2 Turn", Toast.LENGTH_SHORT).show();

                        }
                    });
                    layout_player1.addView(player_one.get(i));
                }

                for(int i = 0; i< player_two.size(); i++)
                {
                    ViewGroup.LayoutParams params2 = new ViewGroup.LayoutParams(60, 80);
                    player_two.get(i).setLayoutParams(params2);
                    final int temp = i;
                    player_two.get(i).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           // open_card_res_id.add(Integer.parseInt(player_two.get(temp).getTag().toString()));
                            if (counter%2!=0) {
                                if (CheckMove(Integer.parseInt(player_two.get(temp).getTag().toString()))) {
                                    openImage.setImageResource(Integer.parseInt(player_two.get(temp).getTag().toString()));
                                    //open_card_res_id.add(Integer.parseInt(player_two.get(temp).getTag().toString()));
                                    open_deck.add(player_two.get(temp));
                                    layout_player2.removeView(player_two.get(temp));
                                    counterPlayer2--;
                                    checkWin();
                                    //    player_two.remove(player_two.get(temp));
                                    counter++;
                                }
                            }
                            else
                                Toast.makeText(getApplicationContext(), "Player 1 Turn", Toast.LENGTH_SHORT).show();

                        }
                    });
                    layout_player2.addView(player_two
                            .get(i));
                }
            }
        });
    }


    public boolean checkWin()
    {
        if(counterPlayer1==0) {
            Toast.makeText(getApplicationContext(), "Player 1 won", Toast.LENGTH_LONG).show();
            layout_player1.setClickable(false);
            layout_player2.setClickable(false);
            openImage.setClickable(false);
            closeImage.setClickable(false);
        }
        else if (counterPlayer2 == 0) {
            Toast.makeText(getApplicationContext(), "Player 2 won", Toast.LENGTH_LONG).show();
            layout_player1.setClickable(false);
            layout_player2.setClickable(false);
            openImage.setClickable(false);
            closeImage.setClickable(false);
        }
        return false;
    }

    public void checkCloseDeck()
    {
        if(closed_deck.isEmpty())
        {
           closed_deck =  open_deck.subList(0, open_deck.size()-2);
        }
    }

    public boolean CheckMove(int imageID)
    {
            int openID = Integer.parseInt(open_deck.get(open_deck.size() - 1).getTag().toString());
            int cardTurnID = imageID;

            String drawopen = getApplicationContext().getResources().getResourceName(openID);
            String drawturn = getApplicationContext().getResources().getResourceName(cardTurnID);

            String splopen[] = drawopen.split("/");
            String splturn[] = drawturn.split("/");

            String turn[] = splturn[1].split("_");
            String open[] = splopen[1].split("_");


            if(open[0].equals(turn[0]))
                return true;

            else if (open[1].equals(turn[1]))
                return true;
            else {
                Toast.makeText(getApplicationContext(), "Invalid Move \n"+open[0]+" "+open[1]
                        +"\n turn"+turn[0]+" "+turn[1], Toast.LENGTH_SHORT).show();
                return false;
            }
    }


    public void startfun()
    {
        for(int i = 0; i<player_one.size(); i++)
        {
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            all_images.get(i).setLayoutParams(params);
            layout_player1.addView(all_images.get(i));
            layout_player1.removeView(all_images.get(i));
        }

        for(int i = 0; i< player_two.size(); i++)
        {
            ViewGroup.LayoutParams params2 = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            all_images.get(i).setLayoutParams(params2);
            layout_player2.addView(all_images.get(i));
        }

    }

}
